import { Route } from '@angular/router'; 
import { EventsShowComponent } from './events-show/events-show.component'; 
import { EventDetailComponent } from './event-detail/event-detail.component'; 
import { EventAddComponent } from './event-add/event-add.component'; 
import { LeavePageGuard } from '../guards/leave-page.guard';
import { EventDetailResolveGuard } from './guards/event-detail-resolve.guard';

export const EVENTS_ROUTES: Route[] = [ 
    { path: '', component: EventsShowComponent }, 
    { 
        path: 'add', 
        component: EventAddComponent,
        canDeactivate: [ LeavePageGuard ]
    }, 
    { 
        path: ':id', 
        component: EventDetailComponent, 
        resolve: {
            event: EventDetailResolveGuard
        }
    }, 
];