import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { IEvent } from '../interfaces/i-event';
import { EventResponse, EventsResponse, HttpErrorResponse, OkResponse } from '../interfaces/responses';
import { SERVICES } from '../../app.constants';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  eventURL: string = SERVICES;

  constructor(private http: HttpClient) { }

  getEvents(): Observable<IEvent[]> { 
    return this.http.get<EventsResponse>(this.eventURL + '/events').pipe(
      map(response => response.events.map(event => {
          event.title = String(event.title);
          event.description = String(event.description);
          return event;
        })
      ), 
      catchError((resp: HttpErrorResponse) => throwError( `Error obteniendo eventos. Código de servidor: ${resp.status}. Mensaje: ${resp.message}`) ) 
    ); 
  }

  getEvent(id: number): Observable<IEvent> { 
    return this.http.get<EventResponse>(this.eventURL + '/events/' + id).pipe(
      map(response => {
        if (response.ok === false)
          throw response.error;
        return response.event
      }), 
      catchError((resp: HttpErrorResponse) => throwError( `Error obteniendo eventos. Código de servidor: ${resp.status}. Mensaje: ${resp.message}`) ) 
    ); 
  }

  addEvent(evento: IEvent): Observable<IEvent> { 
    return this.http.post<EventResponse>(this.eventURL + '/events', evento).pipe(
      map(response => {
        if (response.ok === false)
          throw response.errors;
        return response.event; 
      }), 
      catchError((resp: HttpErrorResponse) => throwError( `Error obteniendo eventos. Código de servidor: ${resp.status}. Mensaje: ${resp.message}`) ) 
    ); 
  }

  deleteEvent(evento: IEvent): Observable<boolean> { 
    return this.http.delete<OkResponse>(this.eventURL + '/events/' + evento.id).pipe(
      map(response => {
        if (response.ok === false)
          throw response.error;
        return true; 
      }), 
      catchError((resp: HttpErrorResponse) => throwError( `Error obteniendo eventos. Código de servidor: ${resp.status}. Mensaje: ${resp.message}`) ) 
    ); 
  }
}
