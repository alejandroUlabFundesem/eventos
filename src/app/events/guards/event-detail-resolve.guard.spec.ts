import { TestBed } from '@angular/core/testing';

import { EventDetailResolveGuard } from './event-detail-resolve.guard';

describe('EventDetailResolveGuard', () => {
  let guard: EventDetailResolveGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(EventDetailResolveGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
