import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { IEvent } from '../interfaces/i-event';
import { EventsService } from '../services/events.service';
import { ComponentDeactivate } from '../../interfaces/component-deactivate';

@Component({
  selector: 'app-event-add',
  templateUrl: './event-add.component.html',
  styleUrls: ['./event-add.component.scss']
})
export class EventAddComponent implements OnInit, ComponentDeactivate {

  @Output() eventAdded = new EventEmitter<IEvent>();

  newEvent: IEvent = {
    title: '',
    description: '',
    image: '',
    price: 0,
    date: ''
  };

  constructor(
    private eventsService: EventsService, 
    private router: Router) { }
  canDeactivate(): boolean {
    return confirm("¿Seguro que quieres salir? los cambios se perderán");
  }

  ngOnInit(): void {
  }

  changeImage(fileInput: HTMLInputElement) { 
    if (!fileInput.files || fileInput.files.length === 0) { return; } 

    const reader: FileReader = new FileReader(); 
    reader.readAsDataURL(fileInput.files[0]); 
    reader.addEventListener('loadend', e => { 
      this.newEvent.image = reader.result.toString(); 
    }); 
  }

  addEvent() {
    this.eventsService.addEvent(this.newEvent).subscribe(
      event => {
        this.router.navigate(['/events']);
      },
      errors => errors.forEach(error => console.log(error))
    );
  }
}
