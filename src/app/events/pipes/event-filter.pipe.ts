import { Pipe, PipeTransform } from '@angular/core';
import { IEvent } from '../interfaces/i-event';

@Pipe({
  name: 'eventFilter'
})
export class EventFilterPipe implements PipeTransform {

  transform(eventos: IEvent[], filterBy:string): IEvent[] {
    if (filterBy === '')
      return eventos;

    filterBy = filterBy.toLocaleLowerCase();

    return eventos.filter(evento => 
            evento.title.toLocaleLowerCase().includes(filterBy) ||
            evento.description.toLocaleLowerCase().includes(filterBy));
  }

}
