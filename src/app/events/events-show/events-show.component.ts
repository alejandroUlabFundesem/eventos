import { Component, OnInit } from '@angular/core';
import { IEvent } from '../interfaces/i-event';
import { EventsService } from '../services/events.service';

@Component({
  selector: 'app-events-show',
  templateUrl: './events-show.component.html',
  styleUrls: ['./events-show.component.scss']
})
export class EventsShowComponent implements OnInit {

  filterSearch: string = '';

  eventos: IEvent[] = [];

  constructor(private eventsService: EventsService) { }

  ngOnInit(): void {
    this.eventsService.getEvents().subscribe(
      events => this.eventos = events,
      error => console.log(error)
    );
  }

  orderDate() {
    this.filterSearch = '';
    this.eventos.sort((evento1, evento2) => evento1.date > evento2.date ? 1 : -1);
  }

  orderPrice() {
    this.filterSearch = '';
    this.eventos.sort((evento1, evento2) => evento1.price < evento2.price ? 1 : -1);
  }

  addEvent(newEvent) {
    this.eventos.push(newEvent);
  }

  deleteEvent(evento: IEvent) {
    this.eventos = this.eventos.filter(event => event !== evento);
  }
}
