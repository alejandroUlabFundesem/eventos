import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EVENTS_ROUTES } from './events.routes';
import { EventsShowComponent } from './events-show/events-show.component';
import { EventAddComponent } from './event-add/event-add.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventFilterPipe } from './pipes/event-filter.pipe';
import { EventItemComponent } from './event-item/event-item.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EventsService } from './services/events.service';
import { EventDetailResolveGuard } from './guards/event-detail-resolve.guard';

@NgModule({
  declarations: [
    EventsShowComponent,
    EventFilterPipe,
    EventItemComponent,
    EventAddComponent,
    EventDetailComponent,
  ],
  imports: [
    CommonModule, 
    FormsModule, 
    HttpClientModule,
    RouterModule.forChild(EVENTS_ROUTES)
  ], 
  providers: [
    EventsService,
    EventDetailResolveGuard
  ]
})
export class EventsModule { }
