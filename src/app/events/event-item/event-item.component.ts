import { EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';
import { IEvent } from '../interfaces/i-event';
import { EventsService } from '../services/events.service';

@Component({
  selector: 'app-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.scss']
})
export class EventItemComponent implements OnInit {

  @Input() evento: IEvent; 
  @Output() eventDeleted = new EventEmitter<void>();

  constructor(private eventsService: EventsService) { }

  ngOnInit(): void {
  }

  deleteEvent() {
    this.eventsService.deleteEvent(this.evento).subscribe(
      ok => this.eventDeleted.emit(),
      error => console.log(error)
    );
  }
}
