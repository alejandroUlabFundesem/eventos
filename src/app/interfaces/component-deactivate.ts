export interface ComponentDeactivate {
    canDeactivate() : boolean;
}
